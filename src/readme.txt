Runescape Old School: About executable script
---------------------------------------------

This script (runescape.sh) was created by Carlos Donizete Froes and distributed
under the BSD-2-Clause license.

The existence of this basic script allows the "RuneScape Old School" client
to run on unofficial GNU/Linux distributions.

The game works on several different platforms (supported or not by Jagex) using
Java installed on the system, allowing the integration of known fixes necessary
for common problems encountered by players.

The script will download the "jagexappletviewer.jar" file from
the official client and then prepare the sandbox environment located
at "$HOME/.local/share/runescape" and run the game.
